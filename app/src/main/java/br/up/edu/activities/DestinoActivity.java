package br.up.edu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class DestinoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        //Recupero a intenção.
        Intent intent = getIntent();

        //Recupero o texto enviado.
        String mensagem = intent.getStringExtra("identificador");

        //Pega o componente e envia a mensagem para o usuário.
        EditText txtMensagem = (EditText) findViewById(R.id.txtMensagem);
        txtMensagem.setText(mensagem);

    }
}

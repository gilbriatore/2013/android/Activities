package br.up.edu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class OrigemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origem);
    }


    public void ir(View v){

        //Declaro a intenção
        Intent intencao = new Intent(this, DestinoActivity.class);
        String texto = "A";
        intencao.putExtra("identificador", texto);
        //Chamo a activity
        startActivity(intencao);

    }

}
